package ru.tsc.avramenko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.avramenko.tm.component.Bootstrap;
import ru.tsc.avramenko.tm.marker.SoapCategory;

import java.util.List;

public class ProjectEndpointTest {

    @NotNull
    protected static final Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private static Session session;

    @Nullable
    private Project project;

    @BeforeClass
    public static void beforeClass() {
        session = bootstrap.getSessionEndpoint().openSession("User", "User");
        bootstrap.getProjectEndpoint().clearProject(session);
    }

    @Before
    public void before() {
        bootstrap.getProjectEndpoint().createProject(session, "Project1", "ProjectDesc1");
        this.project = bootstrap.getProjectEndpoint().findProjectByName(session, "Project1");
    }

    @After
    public void after() {
        bootstrap.getProjectEndpoint().clearProject(session);
    }

    @AfterClass
    public static void afterClass() {
        bootstrap.getSessionEndpoint().closeSession(session);
    }

    @Test
    @Category(SoapCategory.class)
    public void findProjectAll() {
        @NotNull final List<Project> projects = bootstrap.getProjectEndpoint().findProjectAll(session);
        Assert.assertNotNull(projects);
    }

    @Test
    @Category(SoapCategory.class)
    public void finishProjectById() {
        @NotNull final Project projectNew = bootstrap.getProjectEndpoint().finishProjectById(session, project.getId());
        Assert.assertNotNull(projectNew);
        Assert.assertEquals(Status.COMPLETED, projectNew.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void finishProjectByName() {
        @NotNull final Project projectNew = bootstrap.getProjectEndpoint().finishProjectByName(session, project.getName());
        Assert.assertNotNull(projectNew);
        Assert.assertEquals(Status.COMPLETED, projectNew.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void finishProjectByIndex() {
        @NotNull final Project project = bootstrap.getProjectEndpoint().finishProjectByIndex(session, 1);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void changeProjectStatusById() {
        bootstrap.getProjectEndpoint().setProjectStatusById(session, project.getId(), Status.NOT_STARTED);
        @NotNull final Project projectNew = bootstrap.getProjectEndpoint().findProjectById(session, project.getId());
        Assert.assertNotNull(projectNew);
        Assert.assertEquals(Status.NOT_STARTED, projectNew.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void changeProjectStatusByName() {
        bootstrap.getProjectEndpoint().setProjectStatusByName(session, project.getName(), Status.NOT_STARTED);
        @NotNull final Project projectNew = bootstrap.getProjectEndpoint().findProjectById(session, project.getId());
        Assert.assertNotNull(projectNew);
        Assert.assertEquals(Status.NOT_STARTED, projectNew.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void changeProjectStatusByIndex() {
        bootstrap.getProjectEndpoint().setProjectStatusByIndex(session, 1, Status.NOT_STARTED);
        @NotNull final Project projectNew = bootstrap.getProjectEndpoint().findProjectById(session, project.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void startProjectById() {
        @NotNull final Project projectNew = bootstrap.getProjectEndpoint().startProjectById(session, project.getId());
        Assert.assertNotNull(projectNew);
        Assert.assertEquals(Status.IN_PROGRESS, projectNew.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void startProjectByName() {
        @NotNull final Project projectNew = bootstrap.getProjectEndpoint().startProjectByName(session, project.getName());
        Assert.assertNotNull(projectNew);
        Assert.assertEquals(Status.IN_PROGRESS, projectNew.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void startProjectByIndex() {
        @NotNull final Project project = bootstrap.getProjectEndpoint().startProjectByIndex(session, 1);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void updateProjectById() {
        @NotNull final Project projectNew = bootstrap.getProjectEndpoint().updateProjectById(session, project.getId(), "NewProjectName", "NewProjectDesc");
        Assert.assertNotNull(projectNew);
        Assert.assertEquals("NewProjectName", projectNew.getName());
        Assert.assertEquals("NewProjectDesc", projectNew.getDescription());
    }

}