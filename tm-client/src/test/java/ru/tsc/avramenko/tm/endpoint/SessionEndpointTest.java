package ru.tsc.avramenko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.avramenko.tm.component.Bootstrap;
import ru.tsc.avramenko.tm.marker.SoapCategory;

public class SessionEndpointTest {

    @NotNull
    protected static final Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private Session session;

    @Test
    @Category(SoapCategory.class)
    public void openSession() {
        this.session = bootstrap.getSessionEndpoint().openSession("User", "User");
        Assert.assertNotNull(session);
    }

    @Test
    @Category(SoapCategory.class)
    public void closeSession() {
        this.session = bootstrap.getSessionEndpoint().openSession("User", "User");
        @NotNull final boolean result = bootstrap.getSessionEndpoint().closeSession(this.session);
        Assert.assertTrue(result);
    }

}