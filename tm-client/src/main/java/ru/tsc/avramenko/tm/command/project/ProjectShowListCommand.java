package ru.tsc.avramenko.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.command.AbstractProjectCommand;
import ru.tsc.avramenko.tm.endpoint.Project;
import ru.tsc.avramenko.tm.endpoint.Role;
import ru.tsc.avramenko.tm.endpoint.Session;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class ProjectShowListCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show a list of projects.";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSessionService().getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        @NotNull List<Project> projects;
        projects = serviceLocator.getProjectEndpoint().findProjectAll(session);
        int index = 1;
        for (Project project : projects) {
            System.out.println("\n" + "|--- Project [" + index + "]---|");
            showProject(project);
            index++;
        }
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}