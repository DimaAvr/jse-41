package ru.tsc.avramenko.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.command.AbstractCommand;
import ru.tsc.avramenko.tm.endpoint.Session;

public class LogoutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "User logout from system.";
    }

    @Override
    public void execute() {
        @Nullable final boolean logout = serviceLocator.getSessionEndpoint().closeSession(serviceLocator.getSessionService().getSession());
        if (logout) serviceLocator.getSessionService().setSession(null);
    }

}