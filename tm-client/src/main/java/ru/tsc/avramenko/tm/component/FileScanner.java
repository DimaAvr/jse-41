package ru.tsc.avramenko.tm.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.service.IPropertyService;
import ru.tsc.avramenko.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class FileScanner {

    @NotNull
    private static final String PATH = "./";

    @Nullable
    private final int INTERVAL;

    @NotNull
    private final ThreadFactory threadFactory = runnable -> new Thread(runnable, "ScannerThread");

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor(threadFactory);

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final Bootstrap bootstrap;

    public FileScanner(@NotNull Bootstrap bootstrap, @NotNull final IPropertyService propertyService) {
        this.bootstrap = bootstrap;
        this.INTERVAL = propertyService.getFileScannerInterval();
    }

    public void init() {
        commands.addAll(
                bootstrap.getCommandService().getArguments().stream()
                    .map(AbstractCommand::name)
                    .collect(Collectors.toList())
        );
        es.scheduleWithFixedDelay(this::run, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void run() {
        @NotNull final File file = new File(PATH);
        Arrays.stream(file.listFiles())
                .filter(o->o.isFile()&&commands.contains(o.getName()))
                .forEach(o-> {
                    bootstrap.parseCommand(o.getName());
                    o.delete();
                });
    }

    public void stop() {
        es.shutdown();
    }

}