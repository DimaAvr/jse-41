package ru.tsc.avramenko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.endpoint.IAdminDataEndpoint;
import ru.tsc.avramenko.tm.api.service.IServiceLocator;
import ru.tsc.avramenko.tm.component.Backup;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.dto.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AdminDataEndpoint extends AbstractEndpoint implements IAdminDataEndpoint {

    private Backup backup;

    public AdminDataEndpoint() {
        super(null);
    }

    public AdminDataEndpoint(
            @NotNull final IServiceLocator serviceLocator,
            @NotNull final Backup backup
    ) {
        super(serviceLocator);
        this.backup = backup;
    }

    @Override
    @Nullable
    @WebMethod
    public void loadBackup(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        backup.load();
    }

    @Override
    @Nullable
    @WebMethod
    public void saveBackup(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        backup.run();
    }

    @Override
    @Nullable
    @WebMethod
    public void loadDataBase64(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().loadDataBase64();
    }

    @Override
    @Nullable
    @WebMethod
    public void saveDataBase64(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().saveDataBase64();
    }

    @Override
    @Nullable
    @WebMethod
    public void loadDataBin(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().loadDataBin();
    }

    @Override
    @Nullable
    @WebMethod
    public void saveDataBin(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().saveDataBin();
    }

    @Override
    @Nullable
    @WebMethod
    public void loadDataJson(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().loadDataJson();
    }

    @Override
    @Nullable
    @WebMethod
    public void saveDataJson(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().saveDataJson();
    }

    @Override
    @Nullable
    @WebMethod
    public void loadDataXml(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().loadDataXml();
    }

    @Override
    @Nullable
    @WebMethod
    public void saveDataXml(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().saveDataXml();
    }

    @Override
    @Nullable
    @WebMethod
    public void loadDataYaml(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().loadDataYaml();
    }

    @Override
    @Nullable
    @WebMethod
    public void saveDataYaml(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().saveDataYaml();
    }

    @Override
    @Nullable
    @WebMethod
    public void loadDataJsonJaxB(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().loadDataJsonJaxB();
    }

    @Override
    @Nullable
    @WebMethod
    public void saveDataJsonJaxB(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().saveDataJsonJaxB();
    }

    @Override
    @Nullable
    @WebMethod
    public void loadDataXmlJaxB(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().loadDataXmlJaxB();
    }

    @Override
    @Nullable
    @WebMethod
    public void saveDataXmlJaxB(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().saveDataXmlJaxB();
    }

}