package ru.tsc.avramenko.tm.api;

import ru.tsc.avramenko.tm.dto.AbstractEntity;

public interface IService <E extends AbstractEntity> extends IRepository<E> {
}