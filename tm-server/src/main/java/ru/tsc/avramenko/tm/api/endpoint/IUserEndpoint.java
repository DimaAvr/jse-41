package ru.tsc.avramenko.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.avramenko.tm.dto.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IUserEndpoint {

    @WebMethod
    boolean existsUserByEmail(
            @WebParam(name = "email", partName = "email") @NotNull String email
    );

    @WebMethod
    boolean existsUserByLogin(
            @WebParam(name = "login", partName = "login") @NotNull String login
    );

    @WebMethod
    Session registryUser(
            @WebParam(name = "login", partName = "login") @NotNull String login,
            @WebParam(name = "password", partName = "password") @NotNull String password,
            @WebParam(name = "email", partName = "email") @NotNull String email
    );

}