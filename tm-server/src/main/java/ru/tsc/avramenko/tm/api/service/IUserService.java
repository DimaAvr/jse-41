package ru.tsc.avramenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.dto.User;
import java.util.List;

public interface IUserService {

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findById(@Nullable String id);

    void removeByLogin(@Nullable String login);

    void removeById(@Nullable String id);

    void clear();

    void addAll(@Nullable List<User> users);

    @Nullable
    List<User> findAll();

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    User setRole(@Nullable String id, @Nullable Role role);

    boolean isLoginExist(@NotNull String login);

    boolean isEmailExist(@NotNull String email);

    @NotNull
    User updateUserById(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName, @Nullable String email);

    @NotNull
    User updateUserByLogin(@Nullable String login, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName, @Nullable String email);

    @NotNull
    User lockUserByLogin(@Nullable String login);

    @NotNull
    User unlockUserByLogin(@Nullable String login);

}