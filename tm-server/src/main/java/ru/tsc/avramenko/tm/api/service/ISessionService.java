package ru.tsc.avramenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.dto.Session;

import java.util.List;

public interface ISessionService {

    boolean checkDataAccess(@Nullable String login, @Nullable String password);

    @Nullable Session sign(@Nullable Session session);

    @Nullable
    void close(@NotNull Session session);

    @Nullable
    Session open(@Nullable String login, @Nullable String password);

    void validate(@NotNull Session session, @Nullable Role role);

    void validate(@Nullable Session session);

    @Nullable
    List<Session> findAll();

    @Nullable
    Session findById(@Nullable String id);

}