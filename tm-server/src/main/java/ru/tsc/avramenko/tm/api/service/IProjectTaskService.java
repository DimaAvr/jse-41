package ru.tsc.avramenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.dto.Task;

import java.util.List;

public interface IProjectTaskService {

    @NotNull
    List<Task> findTaskByProjectId(@NotNull String userId, @Nullable String projectId);

    @NotNull
    Task bindTaskById(@NotNull String userId, @Nullable String projectId, @Nullable String taskId);

    @NotNull
    Task unbindTaskById(@NotNull String userId, @Nullable String projectId, @Nullable String taskId);

    @Nullable
    void removeProjectById(@NotNull String userId, @Nullable String projectId);

    @Nullable
    void removeProjectByIndex(@NotNull String userId, @Nullable Integer index);

    @Nullable
    void removeProjectByName(@NotNull String userId, @Nullable String name);

}