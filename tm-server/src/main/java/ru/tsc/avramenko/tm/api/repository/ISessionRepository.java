package ru.tsc.avramenko.tm.api.repository;

import org.apache.ibatis.annotations.*;
import ru.tsc.avramenko.tm.dto.Session;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO `app_session`" +
            "(`id`, `timestamp`, `signature`, `user_id`)" +
            "VALUES(#{id},#{timestamp},#{signature},#{userId})")
    void add(
            final Session session);

    @Update("UPDATE `app_session` " +
            "SET `timestamp`=#{timestamp}, `signature`=#{signature}, `user_id`=#{userId} WHERE `id` = #{id}")
    void update(
            final Session session);

    @Delete("DELETE FROM `app_session` WHERE `user_id` = #{userId}")
    void remove(final Session session);

    @Select("SELECT * FROM `app_session` WHERE `id` = #{id} LIMIT 1")
    @Result(column = "user_id", property = "userId")
    Session findById(final String id);

    @Select("SELECT * FROM `app_session`")
    @Result(column = "user_id", property = "userId")
    List<Session> findAll();

}