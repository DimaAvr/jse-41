package ru.tsc.avramenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.avramenko.tm.component.Bootstrap;
import ru.tsc.avramenko.tm.dto.Project;
import ru.tsc.avramenko.tm.dto.Session;
import ru.tsc.avramenko.tm.dto.Task;

import java.util.List;

public class ProjectTaskServiceTest {

    @Nullable
    private ProjectTaskService projectTaskService;

    @Nullable
    private ProjectService projectService;

    @Nullable
    private TaskService taskService;

    @Nullable
    private SessionService sessionService;

    @NotNull
    private Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private Session session;

    @Nullable
    private Project project;

    @Nullable
    private Task task;

    @NotNull
    protected static final String TEST_TASK_NAME = "TestName";

    @NotNull
    protected static final String TEST_PROJECT_NAME = "TestName";

    @NotNull
    protected static final String TEST_DESCRIPTION_NAME = "TestDescription";

    @Before
    public void before() {
        ConnectionService connectionService = new ConnectionService(new PropertyService());
        sessionService = new SessionService(connectionService, bootstrap);
        taskService = new TaskService(connectionService);
        projectService = new ProjectService(connectionService);
        projectTaskService = new ProjectTaskService(connectionService);
        this.session = sessionService.open("Test", "Test");
        projectService.create(session.getUserId(), TEST_PROJECT_NAME, TEST_DESCRIPTION_NAME);
        this.project = projectService.findByName(session.getUserId(), TEST_PROJECT_NAME);
        taskService.create(session.getUserId(), TEST_TASK_NAME, TEST_DESCRIPTION_NAME);
        this.task = taskService.findByName(session.getUserId(), TEST_TASK_NAME);
    }

    @After
    public void after() {
        taskService.removeByName(session.getUserId(), task.getName());
        projectTaskService.removeProjectByName(session.getUserId(), project.getName());
        sessionService.close(session);
    }

    @Test
    public void add() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());
        Assert.assertNull(task.getProjectId());
        Assert.assertEquals(TEST_TASK_NAME, task.getName());
        Assert.assertEquals(TEST_DESCRIPTION_NAME, task.getDescription());

        @NotNull final Task taskById = taskService.findById(session.getUserId(), task.getId());
        Assert.assertNotNull(taskById);
        Assert.assertEquals(task.getId(), taskById.getId());

        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        Assert.assertEquals(TEST_PROJECT_NAME, project.getName());
        Assert.assertEquals(TEST_DESCRIPTION_NAME, project.getDescription());

        @NotNull final Project projectById = projectService.findById(session.getUserId(), project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(project.getId(), projectById.getId());
    }

    @Test
    public void findTaskByProjectId() {
        @NotNull final List<Task> tasks = projectTaskService.findTaskByProjectId(session.getUserId(), project.getId());
        Assert.assertNotNull(tasks);
    }

    @Test
    public void bindTaskById() {
        task.setProjectId(null);
        @NotNull final Task bindTask = projectTaskService.bindTaskById(session.getUserId(), project.getId(), task.getId());
        Assert.assertNotNull(bindTask);
        Assert.assertNotNull(bindTask.getProjectId());
        Assert.assertEquals(bindTask.getProjectId(), project.getId());
    }

    @Test
    public void unbindTaskById() {
        @NotNull final Task bindTask = projectTaskService.unbindTaskById(session.getUserId(), project.getId(), task.getId());
        Assert.assertNotNull(bindTask);
        Assert.assertNull(bindTask.getProjectId());
    }

}